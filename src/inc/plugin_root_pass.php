<?php

function machine_gen_root_pass($con, $conf, $machine, $ignore_dot_ini_activation='no'){
    if($ignore_dot_ini_activation=='yes' || $conf["root_pass_plugin"]["call_root_password_change"]){

        $script_path = $conf["root_pass_plugin"]["root_password_create_script_path"];
        if(file_exists ($script_path) && is_executable ($script_path) ){
            // Change the root password
            $hex = base64_encode(openssl_random_pseudo_bytes(12, $crypto_strong));
            $cmd = "echo 'root:$hex' | chpasswd";
            send_ssh_cmd($conf, $con, $machine["ipaddr"], $cmd);

            // Record it using the shell script plugin
            $cmd = "$script_path --hostname ".$machine["hostname"]." --root-password $hex";
            $output = array();
            $return_var = 0;
            exec($cmd, $output, $return_var);
        }
    }
}

function machine_forget_root_pass($con, $conf, $machine, $ignore_dot_ini_activation='no'){

    if($ignore_dot_ini_activation=='yes' || $conf["root_pass_plugin"]["call_root_password_change"]){

        $script_path = $conf["root_pass_plugin"]["root_password_delete_script_path"];
        if(file_exists ($script_path) && is_executable ($script_path) ){
            // Delete it using the shell script plugin
            $cmd = "$script_path --hostname ".$machine["hostname"]." --root-password $hex";
            $output = array();
            $return_var = 0;
            exec($cmd, $output, $return_var);
        }
    }
}

?>