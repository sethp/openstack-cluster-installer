#!/bin/sh

set -e
set -x

if ! [ -r /etc/oci-poc/oci-poc.conf ] ; then
	echo "Cannot read /etc/oci-poc/oci-poc.conf"
fi
. /etc/oci-poc/oci-poc.conf

# Check that we really have NUMBER_OF_GUESTS machines available
# before starting anything
check_enough_vms_available () {
	EXPECTED_NUM_OF_SLAVES=${1}

	NUM_VM=$(ocicli -csv machine-list | q -d , -H "SELECT COUNT(*) AS count FROM -")
	if [ ${NUM_VM} -lt ${EXPECTED_NUM_OF_SLAVES} ] ; then
		echo "Num of VM too low... exiting"
		exit 1
	fi
}


check_enough_vms_available $((${NUMBER_OF_GUESTS} - 1))

ocicli machine-set-ipmi C1 yes 192.168.100.1 9002 ipmiusr test
ocicli machine-set-ipmi C2 yes 192.168.100.1 9003 ipmiusr test
ocicli machine-set-ipmi C3 yes 192.168.100.1 9004 ipmiusr test
ocicli machine-set-ipmi C4 yes 192.168.100.1 9005 ipmiusr test
ocicli machine-set-ipmi C5 yes 192.168.100.1 9006 ipmiusr test
ocicli machine-set-ipmi C6 yes 192.168.100.1 9007 ipmiusr test
ocicli machine-set-ipmi C7 yes 192.168.100.1 9008 ipmiusr test
ocicli machine-set-ipmi C8 yes 192.168.100.1 9009 ipmiusr test
ocicli machine-set-ipmi C9 yes 192.168.100.1 9010 ipmiusr test
ocicli machine-set-ipmi CA yes 192.168.100.1 9011 ipmiusr test
ocicli machine-set-ipmi CB yes 192.168.100.1 9012 ipmiusr test
ocicli machine-set-ipmi CC yes 192.168.100.1 9013 ipmiusr test
ocicli machine-set-ipmi CD yes 192.168.100.1 9014 ipmiusr test
ocicli machine-set-ipmi CE yes 192.168.100.1 9015 ipmiusr test
ocicli machine-set-ipmi CF yes 192.168.100.1 9016 ipmiusr test
ocicli machine-set-ipmi D0 yes 192.168.100.1 9017 ipmiusr test
ocicli machine-set-ipmi D1 yes 192.168.100.1 9018 ipmiusr test
ocicli machine-set-ipmi D2 yes 192.168.100.1 9019 ipmiusr test
ocicli machine-set-ipmi D3 yes 192.168.100.1 9020 ipmiusr test
ocicli machine-set-ipmi D4 yes 192.168.100.1 9021 ipmiusr test
ocicli machine-set-ipmi D5 yes 192.168.100.1 9022 ipmiusr test
ocicli machine-set-ipmi D6 yes 192.168.100.1 9023 ipmiusr test
ocicli machine-set-ipmi D7 yes 192.168.100.1 9023 ipmiusr test

ocicli swift-region-create bdb
ocicli swift-region-create ver
ocicli swift-region-create pub

ocicli location-create ver-zone-1 ver
ocicli location-create ver-zone-2 ver
ocicli location-create bdb-zone-1 bdb
ocicli location-create bdb-zone-2 bdb
ocicli location-create bdb-zone-3 bdb
ocicli location-create public pub

ocicli network-create ver-s1-net01 192.168.101.0 24 ver-zone-1 no
ocicli network-create ver-s1-net02 192.168.107.0 24 ver-zone-2 no
ocicli network-create bdb-s1-net01 192.168.103.0 24 bdb-zone-1 no
ocicli network-create bdb-s1-net02 192.168.104.0 24 bdb-zone-2 no
ocicli network-create bdb-s1-net03 192.168.105.0 24 bdb-zone-3 no
ocicli network-create public 192.168.106.0 24 public yes

# Set the IPMI network
if [ "${USE_AUTOMATIC_IPMI_SETUP}" = "yes" ] ; then
	ocicli network-create ipmi 192.168.200.0 24 bdb-zone-1 no
	ocicli network-set ipmi --role ipmi --ipmi-match-addr 192.168.0.0 --ipmi-match-cidr 16
	ssh ${HOST_NETWORK_PREFIX}.2 "sed -i s/automatic_ipmi_numbering=no/automatic_ipmi_numbering=yes/ /etc/openstack-cluster-installer/openstack-cluster-installer.conf"
	ssh ${HOST_NETWORK_PREFIX}.2 "mkdir -p /var/www/.ssh"
	ssh ${HOST_NETWORK_PREFIX}.2 "chown www-data:www-data /var/www/.ssh"
fi

ocicli cluster-create s1 cloud.example.com

ocicli network-add ver-s1-net01 s1 all eth1 none
ocicli network-add ver-s1-net02 s1 all eth0 none
ocicli network-add bdb-s1-net01 s1 all eth1 none
ocicli network-add bdb-s1-net02 s1 all eth3 none
ocicli network-add bdb-s1-net03 s1 all eth1 none
ocicli network-add public s1 all eth0 none

ocicli machine-add C1 s1 controller ver-zone-1
ocicli machine-add C2 s1 controller bdb-zone-1
ocicli machine-add C3 s1 controller bdb-zone-2

ocicli machine-add C4 s1 swiftproxy ver-zone-1
ocicli machine-add C5 s1 swiftproxy bdb-zone-1
ocicli machine-add C6 s1 swiftproxy bdb-zone-2

# Store, replica 1
ocicli machine-add CA s1 swiftstore ver-zone-1
ocicli machine-add CB s1 swiftstore ver-zone-2
ocicli machine-add CC s1 swiftstore bdb-zone-1
ocicli machine-add CD s1 swiftstore bdb-zone-2
ocicli machine-add CE s1 swiftstore bdb-zone-3

# Store, replica 2
ocicli machine-add CF s1 swiftstore ver-zone-1
ocicli machine-add D0 s1 swiftstore ver-zone-2
ocicli machine-add D1 s1 swiftstore bdb-zone-1
ocicli machine-add D2 s1 swiftstore bdb-zone-2
ocicli machine-add D3 s1 swiftstore bdb-zone-3

# This this, as it's nicer for debug
ocicli cluster-set s1 --swift-proxy-hostname s1-swiftproxy-1.cloud.example.com

#ocicli swift-calculate-ring s1

exit 0
