#!/bin/sh

set -e
#set -x

unset SSH_AUTH_SOCK

usage (){
	echo "$0 <CLUSTER_NAME>"
}

if [ $# != 1 ] || [ "${1}" = '-h' ] ; then
	usage
	exit 1
fi

CLUSTER_NAME=${1}

CLUSTER_DOMAIN=$(ocicli -csv cluster-show ${CLUSTER_NAME} | grep "Domain:" | cut -d, -f2)
VIP_FQDN=$(ocicli -csv cluster-show ${CLUSTER_NAME} | grep "VIP Hostname:" | cut -d, -f2)
if [ -z "${VIP_FQDN}" ] ; then
	VIP_FQDN=${CLUSTER_NAME}-api.${CLUSTER_DOMAIN}
fi

CLUSTER_ID=$(ocicli -csv cluster-list | q -H -d, "SELECT id FROM - WHERE name='"${CLUSTER_NAME}"'")

SCP="scp -i /etc/openstack-cluster-installer/id_rsa"
SSH="ssh -i /etc/openstack-cluster-installer/id_rsa"

for res in $(ocicli -csv machine-list --all | q -H -d, "SELECT hostname, role FROM - WHERE status='installed' AND cluster='"${CLUSTER_NAME}"'") ; do
	SRV=$(echo $res | awk -F ',' '{ print $1 }')
	ROLE=$(echo $res | awk -F ',' '{ print $2 }')
	echo "===> Updating $SRV"
	ROLE=$(echo $SRV | cut -d. -f1 | cut -d- -f2)
	echo "-> Machine is a $ROLE"

	echo "-> Copying root CA"
	${SCP} /var/lib/oci/ssl/ca/oci-pki-oci-ca-chain.pem $SRV:/etc/ssl/certs/oci-pki-oci-ca-chain.pem
	${SCP} /var/lib/oci/ssl/ca/oci-pki-oci-ca.pem       $SRV:/etc/ssl/certs/oci-pki-oci-ca.pem
	${SCP} /var/lib/oci/ssl/ca/oci-pki-root-ca.pem      $SRV:/etc/ssl/certs/oci-pki-root-ca.pem

	if [ "$ROLE" = "controller" ] || [ "$ROLE" = "swiftproxy" ]; then
	echo "-> Copying API key and certs"
		${SCP} /var/lib/oci/ssl/slave-nodes/${VIP_FQDN}/${VIP_FQDN}.crt $SRV:/etc/ssl/certs/oci-pki-api.crt
		${SCP} /var/lib/oci/ssl/slave-nodes/${VIP_FQDN}/${VIP_FQDN}.csr $SRV:/etc/ssl/certs/oci-pki-api.csr
		${SCP} /var/lib/oci/ssl/slave-nodes/${VIP_FQDN}/${VIP_FQDN}.pem $SRV:/etc/ssl/private/oci-pki-api.pem
		${SCP} /var/lib/oci/ssl/slave-nodes/${VIP_FQDN}/${VIP_FQDN}.key $SRV:/etc/ssl/private/oci-pki-api.key
	fi
	if [ "$ROLE" = "swiftproxy" ]; then
		${SCP} /var/lib/oci/ssl/slave-nodes/${VIP_FQDN}/${VIP_FQDN}.key $SRV:/etc/ssl/private/oci-pki-swiftproxy.key
		${SCP} /var/lib/oci/ssl/slave-nodes/${VIP_FQDN}/${VIP_FQDN}.pem $SRV:/etc/ssl/private/oci-pki-swiftproxy.pem
	fi

	echo "-> Copy the node's certs"
	${SCP} /var/lib/oci/ssl/slave-nodes/$SRV/$SRV.key $SRV:/etc/ssl/private/ssl-cert-snakeoil.key
	${SCP} /var/lib/oci/ssl/slave-nodes/$SRV/$SRV.crt $SRV:/etc/ssl/certs/ssl-cert-snakeoil.pem

	echo "===> Restarting services on $SRV"
	${SCP} /usr/bin/oci-update-cluster-certs-restart-services $SRV:/usr/bin
	${SSH} $SRV /usr/bin/oci-update-cluster-certs-restart-services
done
