# module_name/lib/facter/ssh_host_cert.rb
Facter.add(:oci_ssh_host_key_certs_list) do
  setcode do
    Dir.chdir("/etc/ssh")
    files = Dir.glob("ssh_host_*_key-cert.pub")
    files.each_with_index do |file, index|
      files[index] = "/etc/ssh/" + file
    end
    files
  end
end
